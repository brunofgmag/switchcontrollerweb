<?php
/**
 * Created by PhpStorm.
 * User: brunomagalhaes
 * Date: 22/10/18
 * Time: 02:35
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "db/db.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "model/switch.php");

class SwitchDAO {
    private $db;

    /**
     * SwitchDAO constructor.
     */
    public function __construct()
    {
        $this->db = Database::dbConnect();
    }

    /**
     * @param SwitchModel $switch_model
     * @return bool
     * @throws Exception
     */
    public function add(SwitchModel $switch_model) {
        $device_port = $switch_model->getDevicePort();
        $name = $switch_model->getName();
        $desc = $switch_model->getDesc();
        $cycle = $switch_model->getCycle();

        $statement = $this->db->prepare('INSERT INTO switches(device_port, name, desc, cycle) VALUES (:device_port, :name, :description, :cycle)');

        if(!$statement) {
            throw new Exception('[SwitchDAO.add] Query error(' . $this->db->lastErrorCode() . '):' . $this->db->lastErrorMsg() . '');
        }

        $statement->bindValue(':device_port', $device_port, SQLITE3_INTEGER);
        $statement->bindValue(':name', $name, SQLITE3_TEXT);
        $statement->bindValue(':description', $desc, SQLITE3_TEXT);
        $statement->bindValue(':cycle', $cycle, SQLITE3_INTEGER);

        $result = $statement->execute();

        if(!$result) {
            if($this->db->lastErrorCode() == 19) {
                throw new Exception('Já existe um switch com esse device_port!');
            } else {
                throw new Exception('[SwitchDAO.add] Query error(' . $this->db->lastErrorCode() . '):' . $this->db->lastErrorMsg() . '');
            }
        }

        return true;
    }

    /**
     * @param SwitchModel $switch_model
     * @return bool
     * @throws Exception
     */
    public function removeByObject(SwitchModel $switch_model) {
        $device_port = $switch_model->getDevicePort();

        $statement = $this->db->prepare('DELETE FROM switches WHERE device_port = :device_port');
        $statement->bindValue(':device_port', $device_port, SQLITE3_INTEGER);

        if(!$statement) {
            throw new Exception("[SwitchDAO.get] Query error(" . $this->db->lastErrorCode() . "):" . $this->db->lastErrorMsg());
        }

        $result = $statement->execute();

        if(!$result) {
            die("[SwitchDAO.remByObj] Query error(" . $this->db->lastErrorCode() . "):" . $this->db->lastErrorMsg());
        }

        return true;
    }

    /**
     * @param $device_port
     * @return bool
     * @throws Exception
     */
    public function removeById($device_port) {
        try {
            $this->get($device_port);
        } catch (Exception $e) {
            throw new Exception("Não há switch cadastrado com o id $device_port!");
        }

        $statement = $this->db->prepare('DELETE FROM switches WHERE device_port = :device_port');
        $statement->bindValue(':device_port', $device_port, SQLITE3_INTEGER);

        $result = $statement->execute();

        if(!$result) {
            throw new Exception('Não existe switch com esse id!');
        }

        return true;
    }

    /**
     * @param $device_port
     * @return SwitchModel
     * @throws Exception
     */
    public function get($device_port) {
        $statement = $this->db->prepare('SELECT * FROM switches WHERE device_port = :device_port');
        $statement->bindValue(':device_port', $device_port, SQLITE3_INTEGER);

        $result = $statement->execute();

        if(!$result) {
            throw new Exception("[SwitchDAO.get] Query error(" . $this->db->lastErrorCode() . "):" . $this->db->lastErrorMsg());
        }

        $result = $result->fetchArray();

        if(!$result) {
            throw new Exception('Não existe switch com esse id!');
        }

        $switch_model = new SwitchModel();
        $switch_model->setDevicePort($result['device_port']);
        $switch_model->setName($result['name']);
        $switch_model->setDesc($result['desc']);
        $switch_model->setCycle($result['cycle']);

        return $switch_model;
    }

    /**
     * @param SwitchModel $switch_model
     * @return bool
     * @throws Exception
     */
    public function update(SwitchModel $switch_model) {
        $device_port = $switch_model->getDevicePort();
        $name = $switch_model->getName();
        $desc = $switch_model->getDesc();
        $cycle = $switch_model->getCycle();

        try {
            $this->get($device_port);
        } catch (Exception $e) {
            throw new Exception("Não há switch cadastrado com o id $device_port!");
        }

        $statement = $this->db->prepare('UPDATE switches SET name = :name, desc = :description, cycle = :cycle WHERE device_port = :device_port');

        if(!$statement) {
            throw new Exception('[SwitchDAO.add] Query error(' . $this->db->lastErrorCode() . '):' . $this->db->lastErrorMsg() . '');
        }

        $statement->bindValue(':name', $name, SQLITE3_TEXT);
        $statement->bindValue(':description', $desc, SQLITE3_TEXT);
        $statement->bindValue(':cycle', $cycle, SQLITE3_INTEGER);
        $statement->bindValue(':device_port', $device_port, SQLITE3_INTEGER);

        $result = $statement->execute();

        if(!$result) {
            throw new Exception('[SwitchDAO.add] Query error(' . $this->db->lastErrorCode() . '):' . $this->db->lastErrorMsg() . '');
        }

        return true;
    }


    /**
     * @return ArrayObject
     */
    public function _list() {
        $result = $this->db->query('SELECT * FROM switches');

        if(!$result) {
            die("[SwitchDAO._list] Query error(" . $this->db->lastErrorCode() . "):" . $this->db->lastErrorMsg());
        }

        $switches_list = new ArrayObject();

        while ($row = $result->fetchArray()) {
            $switch_model = new SwitchModel();
            $switch_model->setDevicePort($row['device_port']);
            $switch_model->setName($row['name']);
            $switch_model->setDesc($row['desc']);
            $switch_model->setCycle($row['cycle']);

            $switches_list->append($switch_model);
        }

        return $switches_list;
    }
}