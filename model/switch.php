<?php
/**
 * Created by PhpStorm.
 * User: brunomagalhaes
 * Date: 22/10/18
 * Time: 02:29
 */

class SwitchModel {
    public $device_port;
    public $name;
    public $desc;
    public $cycle;

    /**
     * SwitchModel constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return integer
     */
    public function getDevicePort()
    {
        return $this->device_port;
    }

    /**
     * @param integer $device_port
     */
    public function setDevicePort($device_port): void
    {
        $this->device_port = $device_port;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * @param string $desc
     */
    public function setDesc($desc): void
    {
        $this->desc = $desc;
    }

    /**
     * @return integer
     */
    public function getCycle()
    {
        return $this->cycle;
    }

    /**
     * @param integer $cycle
     */
    public function setCycle($cycle): void
    {
        $this->cycle = $cycle;
    }

    /**
     * @param $array
     * @return SwitchModel
     */
    public static function fromArray($array) {
        $switch_model = new SwitchModel();
        $switch_model->setDevicePort($array->device_port);
        $switch_model->setName($array->name);
        $switch_model->setDesc($array->desc);
        $switch_model->setCycle($array->cycle);

        return $switch_model;
    }
}