<?php
/**
 * Created by PhpStorm.
 * User: brunomagalhaes
 * Date: 22/10/18
 * Time: 03:04
 */

header('Content-Type: application/json');

$_SERVER['DOCUMENT_ROOT'] = __DIR__ . '/';

$route = $_GET['route'];
$_SERVER['id'] = $_GET['id'];

switch ($route) {
    case "add": include_once "routes/add.php"; break;
    case "list": include_once "routes/list.php"; break;
    case "get": include_once  "routes/get.php"; break;
    case "update": include_once "routes/put.php"; break;
    case "delete": include_once "routes/delete.php"; break;
    default:
        $response_array = array(
            "status" => false,
            "message" => "Rota não existente!"
        );

        http_response_code(404);

        die(json_encode($response_array));
        break;
}

class Main{
    static function exec($comando){
        //Executa o comando na placa alvo.
        system($comando);
    }
}