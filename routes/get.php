<?php
/**
 * Created by PhpStorm.
 * User: brunomagalhaes
 * Date: 28/10/18
 * Time: 03:14
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "model/dao/switchdao.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "model/switch.php");

header('Content-Type: application/json');

$switch_dao = new SwitchDAO();
$response_array = null;
$id = $_SERVER['id'];

try {
    $switch = $switch_dao->get($id);
} catch (Exception $e) {
    $response_array = array(
        "status" => false,
        "message" => "O switch com id $id não existe!"
    );

    http_response_code(404);

    die(json_encode($response_array));
}

die(json_encode($switch));