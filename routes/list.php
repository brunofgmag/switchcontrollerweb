<?php
/**
 * Created by PhpStorm.
 * User: brunomagalhaes
 * Date: 28/10/18
 * Time: 02:31
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "model/dao/switchdao.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "model/switch.php");

header('Content-Type: application/json');

$switch_dao = new SwitchDAO();
$response_array = null;

$switch_list = $switch_dao->_list();

foreach ($switch_list as $key => $value) {
    //Aqui terá uma chamada que irá executar o comando na placa alvo usando o método comentado abaixo:
    //Main::exec("comando");
    //echo " [" . $value->device_port . " -> " . $value->cycle . "] ";
}

die(json_encode((array)$switch_list, true));