<?php
/**
 * Created by PhpStorm.
 * User: brunomagalhaes
 * Date: 30/10/18
 * Time: 20:45
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "model/dao/switchdao.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "model/switch.php");

header('Content-Type: application/json');

$switch_dao = new SwitchDAO();
$response_array = null;
$id = $_SERVER['id'];

try {
    $switch = $switch_dao->removeById($id);
} catch (Exception $e) {
    $response_array = array(
        "status" => false,
        "response" => "Não foi possível remover esse switch: " . $e->getMessage()
    );

    http_response_code(404);

    die(json_encode($response_array));
}

http_response_code(200);

$response_array = array(
    "status" => true,
    "message" => "Switch removido com sucesso!"
);

//Aqui terá uma chamada que irá executar o comando na placa alvo usando o método comentado abaixo:
//Main::exec("comando");

die(json_encode($response_array));