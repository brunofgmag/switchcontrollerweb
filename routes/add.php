<?php
/**
 * Created by PhpStorm.
 * User: brunomagalhaes
 * Date: 24/10/18
 * Time: 03:14
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "model/dao/switchdao.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "model/switch.php");

header('Content-Type: application/json');

$switch_dao = new SwitchDAO();
$response_array = null;

try {
    $json_post = file_get_contents('php://input');
    $switch_array = json_decode($json_post);
} catch (Exception $e) {
    $response_array = array(
        "status" => false,
        "message" => "Não foi possível ler o JSON: " . $e->getMessage()
    );

    http_response_code(422);

    die(json_encode($response_array));
}

try {
    $switch_model = SwitchModel::fromArray($switch_array);
} catch (Exception $e) {
    $response_array = array(
        "status" => false,
        "message" => "Não foi possível converter o JSON em objeto: " . $e->getMessage()
    );

    http_response_code(422);

    die(json_encode($response_array));
}

try {
    $switch_dao->add($switch_model);
} catch (Exception $e) {
    $response_array = array(
        "status" => false,
        "message" => "Não foi possível adicionar o switch: " . $e->getMessage()
    );

    http_response_code(422);

    die(json_encode($response_array));
}

http_response_code(200);

$response_array = array(
    "status" => true,
    "message" => "Switch adicionado com sucesso!"
);

//Aqui terá uma chamada que irá executar o comando na placa alvo usando o método comentado abaixo:
//Main::exec("comando");

die(json_encode($response_array));