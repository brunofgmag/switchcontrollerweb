<?php
/**
 * Created by PhpStorm.
 * User: brunomagalhaes
 * Date: 22/10/18
 * Time: 01:13
 */

class Database {
    public static function dbConnect() {
        return new SQLite3($_SERVER['DOCUMENT_ROOT'] . 'db/sqlite.db');
    }
}